# This code was written and Tested in Python 3.7.3
# Pydicom and Requests modules needs to be installed before running
import requests, tarfile, os, shutil, glob, tarfile, sys, time
from pydicom import dicomio
from statistics import mean

# Communication with the Website
url = input("Please enter your url:")
#url = "https://s3.amazonaws.com/viz_data/DM_TH.tgz"
# dwndir = input("Please enter the dir you want to download the files to: ")  # Change this dir according to your desired download directory
expdir = input("Please enter your exported files dir") # change this dir to the exported files dir
#expdir = "/Users/Yuvalezraty/Downloads/DM"
print("Downloading the files..")
dwndir = "/Users/Yuvalezraty/Downloads/DM.tgz"


toolbar_width = 40

# setup toolbar
sys.stdout.write("[%s]" % (" " * toolbar_width))
sys.stdout.flush()
sys.stdout.write("\b" * (toolbar_width + 1))  # return to start of line, after '['

for i in range(toolbar_width):
    time.sleep(6)  # do real work here
    # update the bar
    sys.stdout.write("-")
    sys.stdout.flush()

sys.stdout.write("]\n")  # this ends the progress bar
r = requests.get(url)

# dwndir = "/Users/Yuvalezraty/Downloads/DM.tgz"
with open(dwndir, 'wb') as f:
    f.write(r.content)
if r.status_code == 200:
    print("The files were downloaded Successfully")
else:
    print("Download Error, please check your file directory")

tf = tarfile.open(dwndir)
tf.extractall(path = expdir)

# The dir parser
ldir = []
namedir = []
for name in glob.glob(expdir+"/" + '*.dcm'):
    ldir.append(name)

lpatient = []
lseries = []
linstance = []

for i in range(len(ldir)):
    filename = ldir[i]
    ds = dicomio.read_file(filename)
    expo = expdir + "/" + str(ds.PatientName)
    expo1 = expo + "/" + str(ds.StudyInstanceUID)
    expo2 = str(expo1) + "/" + str(ds.SeriesInstanceUID)
    expo3 = str(expo2) + "/" + os.path.basename(filename)
    if ds.PatientName not in lpatient:
        lpatient.append(ds.PatientName)
    if os.path.exists(expo) == False:
        os.mkdir(expo)
        lpatient.append(ds.PatientName)
    if os.path.exists(expo1) == False:
        os.mkdir(str(expo1))
    if os.path.exists(expo2) == False:
        os.mkdir(str(expo2))
    if os.path.exists(expo3) == False:
        shutil.copyfile(filename, expo3)


# The function that generates and Name, age and sex of the users
files = [f for f in glob.glob(expdir + "/*.dcm", recursive=True)]
pdct = {}
plist = []
for i in range(len(files)):
    filename = ldir[i]
    ds = dicomio.read_file(filename)
    if ds.PatientName not in plist:
        plist.append(ds.PatientName)
        print ("Name:", ds.PatientName, "Sex:", ds.PatientSex, "Age:", ds.PatientAge)

# The function that counts avg scan time, by the telta between start and end time in seconds
avglst = []
for i in range(len(plist)):
    min = 1000000
    max = 0
    for j in range(len(files)):
        filename = ldir[j]
        ds = dicomio.read_file(filename)
        if ds.PatientName == plist[i]:
            if float(ds.AcquisitionTime) > max:
                max = float(ds.AcquisitionTime)
            if float(ds.AcquisitionTime) < min:
                min = float(ds.AcquisitionTime)
    avglst.append(max-min)
print ("The Average Scan Time is:", mean(avglst), "Seconds")

# The function that counts different hospitals
hoslist = []
count = 0
for i in range(len(files)):
    filename = ldir[i]
    ds = dicomio.read_file(filename)
    if ds.InstitutionName not in hoslist:
        hoslist.append(ds.InstitutionName)
        count = count + 1
print ("There are", count, "Different Hospitals")
# The function for acquisition Time Delta
