# This code was written and Tested in Python 3.7.3
# Pydicom and Requests modules needs to be installed before running
import numpy as np, pydicom as pm, requests, tarfile

url = input("Please enter your url:")
print("Downloading the files..")
# url = "https://s3.amazonaws.com/viz_data/DM_TH.tgz"
dwndir = input("Please enter the dir you want to download the files to: ")  # Change this dir according to your desired download directory
r = requests.get(url)
# dwndir = "/Users/Yuvalezraty/Downloads/DM.tgz"
with open(dwndir, 'wb') as f:
    f.write(r.content)
if r.status_code == 200:
    print("The files were downloaded Successfully")
else: "Download Error, please check your file directory"